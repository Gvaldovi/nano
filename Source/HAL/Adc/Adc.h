/****************************************************************************************************/
/**
  \file         Adc.h
  \brief		Adc driver.
  \author       Gerardo Valdovinos
  \project      Nano
  \version      1.0.0
  \date         Apr 6, 2018

  Project compiled with WinAvr. THIS PROJECT HAS A BEER LICENSE.
*/
/****************************************************************************************************/
#ifndef __ADC_H
#define	__ADC_H

/*****************************************************************************************************
*                                            Include files                                           *
*****************************************************************************************************/
#include "AdcCfg.h"
/*****************************************************************************************************
*                                              #DEFINEs                                              *
*****************************************************************************************************/
typedef enum
{
	eADC_CHN0 = 0,
	eADC_CHN1,
	eADC_CHN2,
	eADC_CHN3,
	eADC_CHN4,
	eADC_CHN5,
	eADC_CHN6,
	eADC_CHN7,
	eADC_TEMP,
	eADC_INTERNAL = 14
}teAdcChn;

/*****************************************************************************************************
*                                               #MACROs                                              *
*****************************************************************************************************/
#define ADC_ADDRESS_BASE					0x23u
#define ADC_ADDRESS_OFFSET					3u

#define ADC_PIN(p)							*(u8*)(ADC_ADDRESS_BASE + (ADC_ADDRESS_OFFSET * p) + 0)
#define ADC_PIN_SET(Port, Pin)				ADC_PIN(Port) |= (1 << Pin)
#define ADC_PIN_GET(Port, Pin)				(ADC_PIN(Port) & (1 << Pin)) == (1 << Pin)
#define ADC_PIN_READ(Port)					(u8)ADC_PIN(Port)

#define ADC_DDR(p)							*(u8*)(ADC_ADDRESS_BASE + (ADC_ADDRESS_OFFSET * p) + 1)
#define ADC_DDR_SET(Port, Pin)				ADC_DDR(Port) |= (1 << Pin)

#define ADC_PORT(p)						*(u8*)(ADC_ADDRESS_BASE + (ADC_ADDRESS_OFFSET * p) + 2)
#define ADC_PORT_SET(Port, Pin)			ADC_PORT(Port) |= (1 << Pin)
#define ADC_PORT_RESET(Port, Pin)			ADC_PORT(Port) &= ~(1 << Pin)
#define ADC_PORT_WRITE(Port, Value)		ADC_PORT(Port) = (u8)Value

/*****************************************************************************************************
*                                     Declaration of module TYPEs                                    *
*****************************************************************************************************/

/****************************************************************************************************
*                                   Declaration of module FUNCTIONs                                  *
*****************************************************************************************************/
void vfnAdcInit(const tsAdcDriverCfg *DriverCfg);

void vfnAdcCfg(tsAdcChannelCfg *AdcCfg);
void vfnAdcSet(teAdcPort ePort, u8 u8Pin);
u8 u8fnAdcGet(teAdcPort ePort, u8 u8Pin);
void vfnAdcReset(teAdcPort ePort, u8 u8Pin);
void vfnAdcToggle(teAdcPort ePort, u8 u8Pin);
void vfnAdcWrite(teAdcPort ePort, u8 u8Value);
u8 u8fnAdcRead(teAdcPort ePort);

#endif	/* __ADC_H */
/***************************************End of File**************************************************/
