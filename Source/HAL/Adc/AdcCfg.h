/****************************************************************************************************/
/**
  \file         AdcCfg.h
  \brief		Configuration of Adc driver.
  \author       Gerardo Valdovinos
  \project      Nano
  \version		1.0.0
  \date         Apr 6, 2018

  Project compiled with WinAvr. THIS PROJECT HAS A BEER LICENSE.
*/
/****************************************************************************************************/

#ifndef __ADC_CFG_H
#define	__ADC_CFG_H

/*****************************************************************************************************
*                                            Include files                                           *
*****************************************************************************************************/
#include "Typedefs.h"

/*****************************************************************************************************
*                                              #DEFINEs                                              *
*****************************************************************************************************/

/*****************************************************************************************************
*                                               #MACROs                                              *
*****************************************************************************************************/

/*****************************************************************************************************
*                                     Declaration of module TYPEs                                    *
*****************************************************************************************************/
typedef enum
{
	eADC_REF = 0,
	eADC_AVCC,
	eADC_1_1V = 3,
}teAdcRef;

typedef struct
{
	teAdcRef		eRef;				/* Reference */
}tsAdcChannelCfg;

extern const tsAdcChannelCfg sAdcChannelCfg;
/****************************************************************************************************
*                                   Declaration of module FUNCTIONs                                  *
*****************************************************************************************************/

#endif	/* __ADC_CFG_H */
/***************************************End of File**************************************************/
