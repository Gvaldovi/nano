/****************************************************************************************************/
/**
  \file         I2c.c
  \brief		I2c driver.
  \author       Gerardo Valdovinos
  \project      Nano
  \version      1.0.0
  \date         Aug 12, 2017

  Project compiled with WinAvr. THIS PROJECT HAS A BEER LICENSE.
*/
/****************************************************************************************************/

/*****************************************************************************************************
*                                        Include files                                               *
*****************************************************************************************************/
#include "I2c.h"
#include <avr/interrupt.h>
/*****************************************************************************************************
*                                           #MACROs                                                  *
*****************************************************************************************************/

/*****************************************************************************************************
*                                           #DEFINEs                                                 *
*****************************************************************************************************/

/*****************************************************************************************************
*                                  Declaration of module TYPEs                                       *
*****************************************************************************************************/

/*****************************************************************************************************
*                                     Definition of CONSTs                                           *
*****************************************************************************************************/

/*****************************************************************************************************
*                                    Definition of VARIABLEs                                         *
*****************************************************************************************************/
volatile tsI2cChannel sI2cChannel;			/* I2C channel status */

/*****************************************************************************************************
*                                Declaration of module FUNCTIONs                                     *
*****************************************************************************************************/

/*****************************************************************************************************
*                                        Variables EXTERN                                            *
*****************************************************************************************************/

/*****************************************************************************************************
*                                    Definition of FUNCTIONS                                         *
*****************************************************************************************************/

/*****************************************************************************************************
* \brief    Initialization of I2C driver.
* \author	Gerardo Valdovinos
* \param    void
* \return   void
*****************************************************************************************************/
void vfnI2cInit(const tsI2cChannelCfg *psI2cChannelCfg)
{
	/* Set status structure */
	sI2cChannel.u8Status = I2C_ON;

	/* Configure I2c */
	TWSR = (1 << TWPS0) | (1 << TWIE);		// TWPS (Prescaler) = 1, interrupts enabled

	/* Set Bit rate */
	TWBR = psI2cChannelCfg->eFreq;
}

/*****************************************************************************************************
* \brief    Enable I2C channel.
* \author	Gerardo Valdovinos
* \param    void
* \return   void
*****************************************************************************************************/
void vfnI2cEnable(void)
{
	TWCR |= (1 << TWEN);
}

/*****************************************************************************************************
* \brief    Disable I2C channel.
* \author	Gerardo Valdovinos
* \param    void
* \return   void
*****************************************************************************************************/
void vfnI2cDisable(void)
{
	TWCR &= ~(1 << TWEN);
}

/*****************************************************************************************************
* \brief    Set Transmission Callback.
* \author	Gerardo Valdovinos
* \param    void
* \return   void
*****************************************************************************************************/
void vfnI2cSetCallback(tpvfn pvfnCallback)
{
	sI2cChannel.pvfnCallback = pvfnCallback;
}

/*****************************************************************************************************
* \brief    I2C read function.
* \author	Gerardo Valdovinos
* \param    void
* \return   void
*****************************************************************************************************/
u8 u8fnI2cSend(u16 u16Size, u8 *pu8Data, u8 u8Stop)
{
	if(sI2cChannel.u8Status == I2C_ON)
	{
		/* Copy data to local structure */
		sI2cChannel.u16Size = u16Size;
		sI2cChannel.pu8Data = pu8Data;
		sI2cChannel.u8Stop = u8Stop;
		sI2cChannel.u8Status = I2C_BUSY;

		/* Send START condition and and turn on interrupts. */
		TWCR = (1 << TWINT) | (1 << TWSTA);

		return TRUE;
	}
	else
		return FALSE;
}


/*****************************************************************************************************
* \brief    I2C1 Interrupt Routine Service.
* \author   Gerardo Valdovinos Villalobos
* \param    void
* \return   void
*****************************************************************************************************/
ISR(TWI_vect)
{
	u8 u8Finish = 0;

	if( ((TWSR & 0xF8) == I2C_SLA_ACK) || ((TWSR & 0xF8) == I2C_START) )
	{
		if(sI2cChannel.u16Size--)
			TWDR = *sI2cChannel.pu8Data++;	/* Send data from buffer */
		else
			u8Finish = 1;
	}
	else if((TWSR & 0xF8) == I2C_DATA)
	{
		if(sI2cChannel.u16Size--)
		{
			/* Copy data to buffer */
			*sI2cChannel.pu8Data++ = TWDR;

			/* Send ACK to slave */
			TWCR |= (1 << TWEA);
		}
		else
			u8Finish = 1;
	}

	/* Clear interrupt flags */
	TWCR = (1 << TWINT);

	if(u8Finish)
	{		
		u8Finish = 0;

		/* Verify stop condition */
		if(sI2cChannel.u8Stop)
			TWCR |= (1 << TWSTO);

		/* Finish communication */
		TWCR &= ~(1 << TWINT);
		sI2cChannel.u8Status = I2C_ON;

		/* Callback */
		if(sI2cChannel.pvfnCallback != NULL)
			sI2cChannel.pvfnCallback();
	}
}

/***************************************End of Functions Definition**********************************/
