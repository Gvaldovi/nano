/****************************************************************************************************/
/**
  \file         GpioCfg.h
  \brief		Configuration of GPIO driver.
  \author       Gerardo Valdovinos
  \project      Nano
  \version		1.0.0
  \date         May 03, 2017

  Project compiled with WinAvr. THIS PROJECT HAS A BEER LICENSE.
*/
/****************************************************************************************************/

#ifndef __GPIO_CFG_H
#define	__GPIO_CFG_H

/*****************************************************************************************************
*                                            Include files                                           *
*****************************************************************************************************/
#include "Typedefs.h"

/*****************************************************************************************************
*                                              #DEFINEs                                              *
*****************************************************************************************************/

/*****************************************************************************************************
*                                               #MACROs                                              *
*****************************************************************************************************/

/*****************************************************************************************************
*                                     Declaration of module TYPEs                                    *
*****************************************************************************************************/
typedef enum
{
	eGPIO_PORTB = 0,
	eGPIO_PORTC,
	eGPIO_PORTD,
}teGpioPort;

typedef enum
{
	eGPIO_OUT = 0,
	eGPIO_IN,
	eGPIO_IN_PU,
//	eGPIO_NOT_USED			// Pin not used, activate internal pull-up
}teGpioCfg;

typedef struct
{
	teGpioPort		ePort	: 4;				/* Port */
	u8				u8Pin	: 4;				/* Pin number */
	teGpioCfg		eCfg	: 4;				/* Configuration */
}tsGpioChannelCfg;

typedef struct
{
	u8                  		u8Channels; 	/* Number of channels */
	const tsGpioChannelCfg		*psChannelCfg;  /* Channels configuration pointer */
}tsGpioDriverCfg;

extern const tsGpioDriverCfg sGpioDriverCfg;
/****************************************************************************************************
*                                   Declaration of module FUNCTIONs                                  *
*****************************************************************************************************/

#endif	/* __GPIO_CFG_H */
/***************************************End of File**************************************************/
