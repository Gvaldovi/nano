/****************************************************************************************************/
/**
  \file         Systick.c
  \brief        Manages the system tick that handles task scheduler
  \author       Gerardo Valdovinos
  \project      TaskScheduler
  \version      0.0.1
  \date         Sep 7, 2015

  Program compiled with "gcc-avr",
  Tested and programmed with ATAvrDragon in isp mode.

*/
/****************************************************************************************************/

/*****************************************************************************************************
*                                        Include files                                               *
*****************************************************************************************************/
#include "Systick.h"
#include <avr/interrupt.h>
/*****************************************************************************************************
*                                           #MACROs                                                  *
*****************************************************************************************************/

/*****************************************************************************************************
*                                           #DEFINEs                                                 *
*****************************************************************************************************/

/*****************************************************************************************************
*                                  Declaration of module TYPEs                                       *
*****************************************************************************************************/

/*****************************************************************************************************
*                                     Definition of CONSTs                                           *
*****************************************************************************************************/

/*****************************************************************************************************
*                                    Definition of VARIABLEs                                         *
*****************************************************************************************************/
/* System tick variable */
extern u8 u8SystemTick;

volatile tsSystick asSystick[SYSTICK_CHANNEL_MAX];
/*****************************************************************************************************
*                                Declaration of module FUNCTIONs                                     *
*****************************************************************************************************/

/*****************************************************************************************************
*                                    METADATA INFORMATION                                            *
*****************************************************************************************************/

/*****************************************************************************************************
*                                       Definition of FUNCTIONS                                      *
*****************************************************************************************************/

/*****************************************************************************************************
* \brief    Systick initialization. Timer1 enable for working at 1000 Hz.
* \author   Gerardo Valdovinos
* \param    void
* \return   void
******************************************************************************************************/
void vfnSystickInit(void)
{
    u8 i;

    /* Set up Systick timer to 1ms */
    TCCR0A |= (1 << WGM01);                     // Clear timer on compare match
    TCCR0B |= (1 << CS01) | (1 << CS00);        // Prescaler = 64
    OCR0A = 250;                                // For a 1ms tick. 16MHz / 64 = 250KHz -> (1 / 250KHz) * 250 = 1ms
    TIMSK0 = (1 << OCIE0A);                     // Interrupt enable

    /* Initialize systick struct */
    for(i = 0;i < SYSTICK_CHANNEL_MAX;i++)
    {
        asSystick[i].eMode = SYSTICK_STOP;
        asSystick[i].u8Time = 0;
        asSystick[i].pvfnCallback = NULL;
    }
}

/*****************************************************************************************************
* \brief    Systick oneshot 0. Timer oneshot direct.
* \author   Gerardo Valdovinos
* \param    void
* \return   void
******************************************************************************************************/
u8 u8fnSystickSet(tpvfn pvfnCallback)
{
    u8 i;

    /* Look for available space */
    for(i = 0;i < SYSTICK_CHANNEL_MAX;i++)
    {
        if(asSystick[i].pvfnCallback == NULL)
        {
            asSystick[i].pvfnCallback = pvfnCallback;
            return i;
        }
    }

    return INVALID;
}

/*****************************************************************************************************
* \brief    Interrupt Service Routine for Timer1 (Systick)
* \author   Gerardo Valdovinos
* \param    void
* \return   void
* \notes
******************************************************************************************************/
void vfnSystickStart(teSystickChn eChn, u8 u8Time)
{
    if(eChn < SYSTICK_CHANNEL_MAX)
    {
        asSystick[eChn].u8Time = u8Time;
        asSystick[eChn].eMode = SYSTICK_START;
    }
}

/*****************************************************************************************************
* \brief    Interrupt Service Routine for Timer1 (Systick)
* \author   Gerardo Valdovinos
* \param    void
* \return   void
* \notes
******************************************************************************************************/
void vfnSystickStop(teSystickChn eChn)
{
    if(eChn < SYSTICK_CHANNEL_MAX)
    {
        asSystick[eChn].u8Time = 0;
        asSystick[eChn].eMode = SYSTICK_STOP;
    }
}

/*****************************************************************************************************
* \brief    Interrupt Service Routine for Timer1 (Systick)
* \author   Gerardo Valdovinos
* \param    void
* \return   void
* \notes
******************************************************************************************************/
u8 u8fnSystickTimeGet(teSystickChn eChn)
{
    if(eChn < SYSTICK_CHANNEL_MAX)
        return asSystick[eChn].u8Time;
    else
        return 0;
}

/*****************************************************************************************************
* \brief    Interrupt Service Routine for Timer1 (Systick)
* \author   Gerardo Valdovinos
* \param    void
* \return   void
* \notes
******************************************************************************************************/
ISR(TIMER0_COMPA_vect)
{
    u8 i;

    /* Dispatcher of oneshot timers */
    for(i = 0;i < SYSTICK_CHANNEL_MAX;i++)
    {
        if(asSystick[i].eMode == SYSTICK_START)
        {
            /* Oneshot timers */
            if(asSystick[i].u8Time)
                asSystick[i].u8Time--;
            else
            {
                /* Timer expired */
                asSystick[i].eMode = SYSTICK_STOP;

                if(asSystick[i].pvfnCallback != NULL)
                    asSystick[i].pvfnCallback(); 
            }
        }   
    }

    /* Increment Scheduler counter */
    u8SystemTick++;
}
