/****************************************************************************************************/
/**
  \file         Systick.h
  \brief        Header of Systick.c. Tick for scheduler.
  \author       Gerardo Valdovinos
  \project      TaskScheduler
  \version      0.0.1
  \date         Sep 7, 2015

  Program compiled with "gcc-avr",
  Tested and programmed with ATAvrDragon in isp mode.

*/
/****************************************************************************************************/

#ifndef __SYSTICK_H   /*Duplicated Includes Prevention*/
#define __SYSTICK_H

/*****************************************************************************************************
*                                            Include files                                           *
*****************************************************************************************************/
#include "Typedefs.h"
/*****************************************************************************************************
*                                              #DEFINEs                                              *
*****************************************************************************************************/

/*****************************************************************************************************
*                                               #MACROs                                              *
*****************************************************************************************************/

/*****************************************************************************************************
*                                     Declaration of module TYPEs                                    *
*****************************************************************************************************/
typedef enum
{
    SYSTICK_CHANNEL0 = 0,
    SYSTICK_CHANNEL1,
    SYSTICK_CHANNEL2,
    SYSTICK_CHANNEL_MAX
}teSystickChn;

typedef enum
{
    SYSTICK_STOP = 0,
    SYSTICK_START
}teSystickMode;

typedef struct 
{
    teSystickMode   eMode;
    u8              u8Time;
    tpvfn	        pvfnCallback;
}tsSystick;
/*****************************************************************************************************
*                                          Variables EXTERN                                        *
*****************************************************************************************************/

/*****************************************************************************************************
*                                   Declaration of module FUNCTIONs                                  *
*****************************************************************************************************/
void vfnSystickInit(void);
u8 u8fnSystickSet(tpvfn pvfnCallback);
void vfnSystickStart(teSystickChn eChn, u8 u8Time);
void vfnSystickStop(teSystickChn eChn);
u8 u8fnSystickTimeGet(teSystickChn eChn);

#endif /* __SYSTICK_H */
/***************************************End of File**************************************************/
