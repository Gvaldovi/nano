/****************************************************************************************************/
/**
  \file         Uart.h
  \brief		Uart driver.
  \author       Gerardo Valdovinos
  \project      Nano
  \version      1.0.0
  \date         Sep 12, 2017

  Project compiled with WinAvr. THIS PROJECT HAS A BEER LICENSE.
*/
/****************************************************************************************************/
#ifndef __UART_H
#define	__UART_H

/*****************************************************************************************************
*                                            Include files                                           *
*****************************************************************************************************/
#include "UartCfg.h"
/*****************************************************************************************************
*                                              #DEFINEs                                              *
*****************************************************************************************************/

/*****************************************************************************************************
*                                               #MACROs                                              *
*****************************************************************************************************/

/*****************************************************************************************************
*                                     Declaration of module TYPEs                                    *
*****************************************************************************************************/
typedef struct
{
	u8						u8Status;
	u16						u16TxSize;				/* Tx Size */
	u8*						pu8TxData;				/* Pointer to Tx Data*/
	tpvfn					pvfnTxCallback;			/* Callback for Tx complete */
	u16						u16RxSize;				/* Rx Size */
	u8*						pu8RxData;				/* Pointer to Rx Data */
	tpvfn					pvfnRxCallback;			/* Callback for Rx complete */
}tsUartChannel;
/****************************************************************************************************
*                                   Declaration of module FUNCTIONs                                  *
*****************************************************************************************************/
void vfnUartInit(const tsUartChannelCfg *psUartChannelCfg);
void vfnUartEnable(void);
void vfnUartDisable(void);
void vfnUartSetTxCallback(tpvfn	pvfnCallback);
void vfnUartSetRxCallback(tpvfn	pvfnCallback);
u16 u16fnUartRxSize(void);
u16 u16fnUartRx(u8 *pu8Data);
u8 u8fnUartTx(u16 u16Size, u8 *pu8Data);
void vfnUartRxTimeout(void);

#endif	/* __UART_H */
/***************************************End of File**************************************************/
