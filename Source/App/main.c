/****************************************************************************************************/
/**
  \file         main.c
  \brief
  \author       Gerardo Valdovinos
  \project      Nano
  \version      0.0.1
  \date         Sep 5, 2015

  Program compiled with "gcc-avr",
  Tested and programmed with ATAvrDragon in isp mode.

*/
/****************************************************************************************************/

/*****************************************************************************************************
*                                        Include files                                               *
*****************************************************************************************************/
/* Main includes */
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

/* Application includes */
#include "Services/Typedefs.h"
#include "HAL/Systick/Systick.h"

/*****************************************************************************************************
*                                           #MACROs                                                  *
*****************************************************************************************************/

/*****************************************************************************************************
*                                           #DEFINEs                                                 *
*****************************************************************************************************/
#define F_CPU 16000000UL
/*****************************************************************************************************
*                                  Declaration of module TYPEs                                       *
*****************************************************************************************************/

/*****************************************************************************************************
*                                     Definition of CONSTs                                           *
*****************************************************************************************************/

/*****************************************************************************************************
*                                    Definition of VARIABLEs                                      *
*****************************************************************************************************/

/*****************************************************************************************************
*                                Declaration of module FUNCTIONs                                  *
*****************************************************************************************************/

/*****************************************************************************************************
*                                    METADATA INFORMATION                                        *
*****************************************************************************************************/

/*****************************************************************************************************
*                                       Definition of FUNCTIONS                                      *
*****************************************************************************************************/

/*****************************************************************************************************
* \brief    Main function. Initialization of scheduler and first task (System Task Init).
* \author   Gerardo Valdovinos
* \param    void
* \return   void
* \notes    
******************************************************************************************************/
int main(void)
{
    return 0;
}

